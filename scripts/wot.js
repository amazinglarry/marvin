function getRandomElementFromArray(array) {
    var randomIndex = Math.floor(Math.random() * 10) % array.length;
    return array[randomIndex];
}

var WheelOfTime = function(resp, msgSender) {
    return {
        curses: [
            "Blood and ashes!",
            "Blood and bloody ashes!",
            "Light!",
            "Blasted!",
        ],

        sendMessage: function(message) {
          msgSender.call(resp, message);
        },

        getRandomCurse: function() {
            var curse = getRandomElementFromArray(this.curses);
            return curse;
        },

        curse: function() {
            var curse = this.getRandomCurse();
            this.sendMessage(curse);
        }
    };
}

module.exports = function (robot) {
  robot.hear(/wot curse/, function(resp) {
      new WheelOfTime(resp, resp.send).curse();
  });
}
